const Discord = require("discord.js");
const config = require("./config.json");
const fs = require('fs');

const client = new Discord.Client();

const PREFIX = config.PREFIX;

client.login(config.BOT_TOKEN);

client.once("ready", () => {
  console.log("Ready!");
});

client.once("reconnecting", () => {
  console.log("Reconnecting!");
});

client.once("disconnect", () => {
  console.log("Disconnect!");
});

client.on("message", async message => {
  if (message.author.bot) return;
  if (!message.content.startsWith(PREFIX)) return;

  const args = message.content.split(" ");
  if (message.content.startsWith(`${PREFIX}init`)) {
    init(message, args);
    return;
  } else if (message.content.startsWith(`${PREFIX}update`)) {
    update(message, args);
    return;
  } else if (message.content.startsWith(`${PREFIX}what`)) {
    what(message, args);
    return;
  } else if (message.content.startsWith(`${PREFIX}call`)) {
    call(message, args);
    return;
  } else if (message.content.startsWith(`${PREFIX}ban`)) {
    ban(message, args);
    return;
  } else if (message.content.startsWith(`${PREFIX}unban`)) {
    unban(message, args);
    return;
  } else if (message.content.startsWith(`${PREFIX}list`)) {
    list(message);
    return;
  } else if (message.content.startsWith(`${PREFIX}ranking`)) {
    ranking(message, args);
    return;
  } else if (message.content.startsWith(`${PREFIX}help`)) {
    if (args[1] === 'admin') {
      helpAdmin(message);
    } else {
      help(message);
    }
    return;
  } else if (message.content.startsWith(`${PREFIX}recap`)) {
    recap(message, args);
    return;
  } else if (message.content.startsWith(`${PREFIX}conf`)) {
    conf(message, args);
    return;
  } else {
    message.channel.send("You need to enter a valid command!");
  }
});

/**
 * Point d'entrée aux commandes spécifiques de configuration, réservé aux hauts-gradés
 * @param {*} message 
 */
function conf(message, args) {
  if (!args[1]) {
    message.reply("La commande 'conf' attend un paramètre supplémentaire pour fonctionner. Utiliser '*help' si vous avez besoin d'aide.");
    return;
  }
  message.guild.members.fetch(message.author.id).then(member => {
    if (!member.hasPermission('ADMINISTRATOR')) {
      message.reply("Vous devez posséder les droits d'administration pour accéder aux fonctions de configuration du bot.");
      return;
    } else {
      if (args[1] === 'addGame') {
        addGame(message, args);
        return;
      } else if (args[1] === 'delGame') {
        delGame(message, args);
        return;
      } else if (args[1] === 'updateGame') {
        updateGame(message, args);
        return;
      } else {
        message.channel.send("You need to enter a valid command!");
      }
    }
  }).catch(error => console.log(error));
}

function registerNewGame(gameName, capaciteMax) {
  let rawGamelist = fs.readFileSync('gamelist.json');
  let arrayGames = JSON.parse(rawGamelist);

  gameName = gameName.toLowerCase().replace(" ", "_");

  arrayGames.push({ nom: gameName, maxPlayers: capaciteMax });

  let upData = JSON.stringify(arrayGames);
  fs.writeFileSync('gamelist.json', upData);
}

function addGame(message, args) {
  if (args[2]) {
    if (args[3]) {
      if (Number.isInteger(parseInt(args[3], 10))) {
        registerNewGame(args[2], parseInt(args[3], 10));
        message.reply(args[2] + " a bien été enregistré dans la liste des jeux !");
      } else {
        message.reply("Le 4ème paramètre (nombre de joueurs max) doit être un nombre entier");
        return;
      }
    } else {
      registerNewGame(args[2], 0);
      message.reply(args[2] + " a bien été enregistré dans la liste des jeux ! Pensez à renseigner le nombre max de joueurs !");
    }
  } else {
    message.reply("Le nom du nouveau jeu doit être renseigné !");
  }
}

function delGame(message, args) {
  var gameName = args[2];

  let rawGamelist = fs.readFileSync('gamelist.json');
  let arrayGames = JSON.parse(rawGamelist);

  let newArray = [];

  arrayGames.forEach(g => {
    if (g.nom !== gameName) {
      newArray.push(g);
    }
  })

  let upData = JSON.stringify(newArray);
  fs.writeFileSync('gamelist.json', upData);
  message.reply(gameName + " a bien été supprimé de la liste des jeux !");
}

/**
 * Pas opti, mais flemme
 * @param {*} message 
 * @param {*} args 
 */
function updateGame(message, args) {
  delGame(message, args);
  addGame(message, args);
}

function ranking(message, args) {

  let rawdata = fs.readFileSync('data.json');
  let data = JSON.parse(rawdata);
  let rawGamelist = fs.readFileSync('gamelist.json');
  let arrayGames = JSON.parse(rawGamelist);

  let jeux = [];

  arrayGames.forEach(g => {
    jeux.push({ nom: g.nom, points: 0 });
  });

  data.forEach(d => {
    d.games.forEach(g => {
      jeux.forEach(j => {
        if (g.nom === j.nom) {
          j.points += g.note;
        }
      });
    });
  });
  let reply = "Liste des jeux classés par popularité :\n"
  var result = [];
  for (var i in jeux)
    result.push([i, jeux[i]]);
  result.sort(function (a, b) { return a[1].points - b[1].points }).reverse();;
  var j = 1;
  result.forEach(prop => {
    if (prop[1].points > 0) {
      reply += j + "- " + prop[1].nom + " (" + prop[1].points + " pts)";
      reply += "\n";
    }
    j++;
  });
  message.reply(reply);
}


/**
 * Enregistre (ou ré-) un utilisateur dans le array JSON et le MP pour lui demander une note pour chacun des jeux enregistrés, en attendant une réponse. Toutes les infos sont ainsi stockées dans le data.json
 * @param {*} message 
 */
async function init(message, args) {

  let rawGamelist = fs.readFileSync('gamelist.json');
  let arrayGames = JSON.parse(rawGamelist);
  let rawdata = fs.readFileSync('data.json');
  let data = JSON.parse(rawdata);

  let userGames = [];
  data.forEach(d => {
    if (d.user === message.author.id) {
      userGames = d.games.map(g => g.nom);
    }
  });

  var gamesInit = [];

  for (var i = 0; i < arrayGames.length; i++) {
    var game = arrayGames[i].nom;
    if (!args[1] || args[1] !== 'update' || !userGames.includes(game)) {
      let msg = await message.author.send("Note sur 10 pour " + game + " , 0 étant un non absolu/pas installé, :");
      const filter = m => m.author.id === message.author.id;

      await msg.channel.awaitMessages(filter, {
        max: 1.,
        time: 12000, // time en ms
      }).then(async (collected) => {
        var reply = collected.values().next().value;
        if (Number.isInteger(parseInt(reply.content, 10))) {
          var note = parseInt(reply.content, 10);
          if (note > 10) {
            note = 10;
          }
          if (note < 0) {
            note = 0;
          }
          gamesInit.push({ nom: game, note: note });
        } else {
          msg.reply('La réponse n\'est pas un entier ! Valeur 5 notée par défaut...');
        }

      }).catch(() => {
        // what to do if a user takes too long goes here 
        msg.reply('Trop lent! On va mettre à 5 par défaut');
        gamesInit.push({ nom: game, note: 5 });
      });
    } else {
      gamesInit.push({ nom: game, note: getNoteByGameAndUser(game, message.author.id, data) });
    }
  }

  var result = {
    "user": message.author.id,
    "pseudo": message.author.username,
    "games": gamesInit
  };

  var curData = [];

  data.forEach(user => {
    if (user.user !== message.author.id) {
      curData.push(user);
    }
  });

  curData.push(result);
  let upData = JSON.stringify(curData);
  fs.writeFileSync('data.json', upData);

  if (!args[1] || args[1] !== 'update') {
    message.reply(" Merci de ton enregistrement !");
  }

  message.author.send("Enregistrement des données utilisateur terminé avec succès !");
}

function ban(message, args) {
  let rawdata = fs.readFileSync('data.json');
  let data = JSON.parse(rawdata);

  let userIdList = data.map(d => d.user);

  data.forEach(d => {
    if (d.user === message.author.id) {
      var banAdded = d.bans;
      args.forEach(a => {
        if (userIdList.includes(a)) {
          if (d.bans && d.bans.includes(a)) {
            message.guild.members.fetch(a).then(member => {
              message.author.send(member.nickname + " est déjà exclu !");
            }).catch((error) => {

            });
          } else {
            banAdded.push(a);
            message.guild.members.fetch(a).then(member => {
              message.author.send(member.nickname + " a été exclu de vos futurs *call !");
            }).catch((error) => {

            });
          }
        } else if (!a.startsWith('*')) {
          message.reply(a + " : Cet utilisateur n'est pas connu de WhatTheBot");
        }
      })

      var curData = [];

      data.forEach(user => {
        if (user.user !== message.author.id) {
          curData.push(user);
        }
      });

      var result = {
        "user": d.user,
        "pseudo": d.pseudo,
        "games": d.games,
        "bans": banAdded
      };

      curData.push(result);
      let upData = JSON.stringify(curData);
      fs.writeFileSync('data.json', upData);
    }
  });
}

function unban(message, args) {
  let rawdata = fs.readFileSync('data.json');
  let data = JSON.parse(rawdata);

  let userIdList = data.map(d => d.user);

  data.forEach(d => {
    if (d.user === message.author.id && d.bans) {
      var bans = [];
      d.bans.forEach(b => {
        if (args.includes(b)) {

          message.guild.members.fetch(b).then(member => {
            message.author.send(member.nickname + " a été exclu de vos futurs *call !");
          }).catch((error) => {

          });

        } else {
          bans.push(b);
          message.guild.members.fetch(a).then(member => {
            message.author.send(member.nickname + " n'est pas exclu !");
          }).catch((error) => {

          });
        }
      })

      var curData = [];

      data.forEach(user => {
        if (user.user !== message.author.id) {
          curData.push(user);
        }
      });

      var result = {
        "user": d.user,
        "pseudo": d.pseudo,
        "games": d.games,
        "bans": bans
      };

      curData.push(result);
      let upData = JSON.stringify(curData);
      fs.writeFileSync('data.json', upData);
    }
  });
}

function getNoteByGameAndUser(game, id, data) {

  let note = 0;
  data.forEach(d => {
    if (d.user === id) {
      d.games.forEach(g => {
        if (g.nom === game) {
          note = g.note;
        }
      })
    }
  });
  return note;
}

/**
 * Met à jour la notation d'un jeu pour l'utilisateur ayant effectué la commande
 * @param {*} message 
 */
function update(message, args) {

  if (args.length != 3 || !args[1] || !args[2] || !Number.isInteger(parseInt(args[2], 10))) {
    message.author.send("Arguments invalides. La commande *update doit être formulée comme suit : (*update <nomdujeu:sans_espaces> <note:nombre_entier> )");
    return;
  }

  let argNote = parseInt(args[2], 10);
  let argGame = args[1];

  argGame = argGame.toLowerCase().replace(" ", "_");

  if (argNote > 10) {
    argNote = 10;
  }
  if (argNote < 0) {
    argNote = 0;
  }

  let rawGamelist = fs.readFileSync('gamelist.json');
  let arrayGames = JSON.parse(rawGamelist);
  var games = [];

  let rawdata = fs.readFileSync('data.json');
  let data = JSON.parse(rawdata);

  for (var player in data) {
    if (data[player].user === message.author.id) {
      games = data[player].games;
    }
  }

  var game = games.find(x => x.nom == argGame);
  if (game) {
    game.note = parseInt(argNote, 10);
    message.reply("Mise à jour effectuée avec succès !");
  } else {
    game = arrayGames.find(x => x.nom == argGame);
    if (game) {
      games.push({ nom: game.nom, note: argNote });
      message.reply("Mise à jour effectuée avec succès !");
    } else {
      message.guild.members.fetch(message.author.id).then(member => {
        if (!member.hasPermission('ADMINISTRATOR')) {
          message.reply("Le jeu mentionné n'a pas encore été référencé : Envoi automatique d'un MP au propriétaire du serveur pour demander l'ajout...");
          message.channel.guild.owner.send(message.author.username + " propose d'ajouter le jeu " + argGame + " à la liste des jeux référencés par WhatToPlayBOT. Utilisez la commande :\n*conf addGame " + argGame + " <nbMaxPlayers>\n pour ajouter le jeu.");
          return;
        } else {
          message.reply("Le jeu mentionné n'a pas encore été référencé : Enregistrement automatique... Pensez à renseigner ultérieurement la capacité max de joueurs");
          registerNewGame(argGame, 0);
        }
      }).catch(error => console.log(error));
    }
  }

  var curData = [];

  data.forEach(user => {
    if (user.user !== message.author.id) {
      curData.push(user);
    }
  });

  var result = {
    "user": message.author.id,
    "pseudo": message.author.username,
    "games": games
  };

  curData.push(result);
  let upData = JSON.stringify(curData);
  fs.writeFileSync('data.json', upData);

}

/**
 * Applique un algo alambiqué pour proposer une sélection des jeux jouables par les utilisateurs connectés sur le channel
 * @param {*} message 
 */
function what(message, args) {

  let reply = "Algorithme en version alpha : des améliorations significatives sont prévues dans le futur\n";

  const voiceChannel = message.member.voice.channel;
  if (!voiceChannel) {
    message.reply("Il faut être présent dans un channel vocal pour pouvoir utiliser cette commande !");
    return;
  }

  //Map, itérer sur les keys (id users)
  let playersOnline = voiceChannel.members.map(p => p.user.id);

  let rawdata = fs.readFileSync('data.json');
  let data = JSON.parse(rawdata);
  let rawGamelist = fs.readFileSync('gamelist.json');
  let arrayGames = JSON.parse(rawGamelist);

  let jeuxPotentiels = [];

  arrayGames.forEach(g => {
    if (g.nom === "among_us" && playersOnline.length < 6) {
      //Exceptions notables à la main, du type nb joueurs minimums pour among us
    } else {
      //On exclut les jeux n'ayant pas la capacité d'accueillir suffisamment de gens
      if (g.maxPlayers >= playersOnline.length || g.maxPlayers === 0 || args.includes("noMax")) {
        jeuxPotentiels.push({ nom: g.nom, points: 0 });
      }
    }
  });

  data.forEach(d => {
    if (playersOnline.includes(d.user)) {
      d.games.forEach(g => {
        jeuxPotentiels.forEach(j => {
          if (g.nom === j.nom) {
            j.points += g.note;
            if (g.note === 0) {
              j.points = -999;
            }
          }
        });
      });
    }
  });

  if (args.includes("list")) {
    reply += "Liste des jeux recommandés par affinité décroissante :\n"
    var result = [];
    for (var i in jeuxPotentiels)
      result.push([i, jeuxPotentiels[i]]);
    result.sort(function (a, b) { return a[1].points - b[1].points }).reverse();;
    var j = 1;
    result.forEach(prop => {
      if (prop[1].points > 0) {

        if (j == 1) {
          reply += "\t**Recommandé par WhatTheBot** =>\t__" + prop[1].nom + "__";
        } else {
          reply += j + "- " + prop[1].nom;
        }
        reply += "\n";
      }
      j++;
    })
  } else {

    let premier = "";
    let points1 = 0;
    let deuxieme = "";
    let points2 = 0;
    let troisieme = "";
    let points3 = 0;
    jeuxPotentiels.forEach(j => {
      if (j.points > 0) {
        if (j.points > points1) {
          points3 = points2;
          troisieme = deuxieme;
          points2 = points1;
          deuxieme = premier;
          points1 = j.points;
          premier = j.nom;
        } else if (j.points > points2) {
          points3 = points2;
          troisieme = deuxieme;
          points2 = j.points;
          deuxieme = j.nom;
        } else if (j.points > points3) {
          points3 = j.points;
          troisieme = j.nom;
        }
      }
    });

    if (premier && deuxieme && troisieme) {
      reply += "**WhatTheBot** recommande **__" + premier + "__**, qui correspond le mieux aux critères des joueurs présents ! Sinon, peut-être que **__" + deuxieme + "__** ou **__" + troisieme + "__** conviendraient ?";
    } else if (premier && deuxieme) {
      reply += "**WhatTheBot** recommande **__" + premier + "__**, qui correspond le mieux aux critères des joueurs présents ! Sinon, peut-être que **__" + deuxieme + "__** conviendrait ?";
    } else if (premier) {
      reply += "**WhatTheBot** recommande **__" + premier + "__**, qui correspond le mieux aux critères des joueurs présents !";
    } else {
      reply += "**WhatTheBot** n'a pas trouvé de jeu qui pourrait convenir à tout le monde... Au risque d'en déplaire à certains, vous pouvez lancer la commande '*what no-veto' pour déterminer le jeu le plus adapté, au risque que tous ne puissent pas prendre part à la session de jeu";
    }
  }

  message.reply(reply);
}

//TODO apposer certaines restrictions pour éviter le spam
/**
 * L'utilisateur notifie les autres joueurs d'un certain jeu (avec une note > 5) qu'il veut y jouer
 * @param {*} message 
 */
function call(message, args) {
  if (!args[1]) {
    message.reply("Il faut préciser le nom du jeu !");
    return;
  } else {
    let rawGamelist = fs.readFileSync('gamelist.json');
    let gamelist = JSON.parse(rawGamelist).map(rg => rg.nom);

    if (!gamelist.includes(args[1])) {
      message.reply("Le jeu signalé est inconnu !");
      return;
    }

    let jeu = args[1].toLowerCase().replace(" ", "_");
    let rawdata = fs.readFileSync('data.json');
    let data = JSON.parse(rawdata);

    let doNotSend = [];
    doNotSend.push(message.author.id);

    const voiceChannel = message.member.voice.channel;
    if (voiceChannel) {
      voiceChannel.members.forEach(m => {
        doNotSend.push(m.id);
      })
    }

    data.forEach(d => {
      if (!doNotSend.includes(d.user)) {
        d.games.forEach(g => {
          if (g.nom === jeu && g.note > 5) {
            //envoyer mp
            message.guild.members.fetch(d.user).then(member => {
              if (member.presence.status === 'online' || member.presence.status === 'idle') {
                let msg = "========================================================\nSalut ! **" + message.author.username + "** recherche des partenaires pour jouer à **" + g.nom.replace("_", " ") + "** ! Rejoins-le sur __" + message.channel.guild.name + "__ si tu es intéressé !\n"
                var i = 2;
                while (args[i]) {
                  msg += args[i] + " ";
                  i++;
                }
                msg += "\n*(pour ne pas recevoir d'invitations, mettez-vous en \"Ne pas déranger\" ou signalez-vous aux admins du serveur)*\n========================================================"
                member.send(msg);
                message.reply("Une invitation a été envoyée à " + d.pseudo);
              }
            }).catch(error => {
            });
          }
        });
      }
    });
  }
}

/**
 * Répond avec un récap des jeux de l'utilisateur demandeur et leur notation. Avec un utilisateur connu en argument, peut donner les infos de cet utilisateur à la place
 * @param {*} message 
 */
function recap(message, args) {
  let rawdata = fs.readFileSync('data.json');
  let data = JSON.parse(rawdata);
  let rawGamelist = fs.readFileSync('gamelist.json');
  let gamelist = JSON.parse(rawGamelist).map(rg => rg.nom);
  if (args[1]) {
    let username = message.content.replace("*recap ", "");
    let reply = "Récap des jeux d'" + username + " : ";

    data.forEach(user => {
      if (user.pseudo === username) {
        user.games.forEach(g => {
          if (gamelist.includes(g.nom)) {
            reply += "\n\t# " + g.nom + " | Note : " + g.note;
          }
        })
      }
    });
    message.reply(reply);
  } else {
    let reply = "Récap des jeux d'" + message.author.username + " : ";

    data.forEach(user => {
      if (user.user === message.author.id) {
        user.games.forEach(g => {
          if (gamelist.includes(g.nom)) {
            reply += "\n\t# " + g.nom + " | Note : " + g.note;
          }
        })
      }
    });
    message.reply(reply);
  }
}

/**
 * Liste les jeux connus du bot
 * @param {*} message 
 */
function list(message) {
  let rawGamelist = fs.readFileSync('gamelist.json');
  let arrayGames = JSON.parse(rawGamelist);

  let reply = "Voici la liste des jeux déjà référencés : ";

  arrayGames.forEach(g => {
    reply += "\n\t# " + g.nom + " | Nombre de joueurs max : " + g.maxPlayers;
  });

  message.reply(reply);
}

/**
 * Commande d'aide aux commandes du bot
 * @param {*} message 
 */
function help(message) {
  let reply = "**__Fonctionnement du bot__**\nCe bot propose de gérer un système de préférence personnel pour ses jeux afin de permettre une recherche rapide du jeu en commun le plus adapté lors de sessions de jeu en groupe.\n**Merci d'utiliser la commande \*init dès que possible pour pouvoir utiliser le bot librement.**\nLes notations vont de 0 à 10. La note 0 pose un droit de veto sur le jeu associé (pas installé ou aucune envie d'y jouer).\nLa version alpha actuelle de l'algorithme se base exclusivement sur les notes pour sélectionner le meilleur jeu en cumulant celles fournies par chaque joueur connecté.\n"
  reply += "**__Liste des commandes__**\n------------------\n- __*init__ :\n\t**Usage** : Enregistre l'utilisateur Discord dans le bot. Permet d'initialiser les notes des jeux référencés. Obligatoire pour utiliser pleinement le bot.\n\t**Arguments** : <update>(Optionel) | Fournir le paramètre 'update' permet à l'utilisateur de mettre uniquement à jour les jeux qu'il n'a pas encore noté\n";
  reply += "------------------\n- __*what__ :\n\t**Usage** : Détermine le jeu le plus adapté pour les joueurs connectés sur le channel vocal courant en utilisant un algorithme de la NASA.\n\t**Arguments** : <list>(Optionnel) | Ajouter le paramètre **'list'** permet d'obtenir la liste complète des jeux triés par ordre d'affinité\n\t<noMax>(Optionnel) | Ajouter le paramètre **'noMax'** permet de ne pas tenir compte du nombre maximal possible de joueurs pour la recherche d'un jeu adapté\n";
  reply += "------------------\n- __*help__ :\n\t**Usage** : Affiche la liste détaillée des commandes du bot.\n\t**Arguments** : admin(Optionnel) | Si 'admin' est renseigné, affiche les commandes réservées aux administrateurs.\n";
  message.author.send(reply);

  let reply2 = "------------------\n- __*list__ :\n\t**Usage** : Affiche la liste des jeux connus par le bot.\n\t**Arguments** : aucun\n";
  reply2 += "------------------\n- __*call__ :\n\t**Usage** : Envoie un MP à tous les utilisateurs du serveur enregistrés **sauf hors-ligne ou dot-not-disturb** qui ont donné une note supérieure à 5 au jeu donné, leur proposant une game.\n\t**Arguments** : <nomDuJeu>(Obligatoire) | Nom du jeu voulu, normalisé\n\t\t<messageSupplementaire>(Optionnel) | Message complémentaire qui sera distribué en plus de l'invitation classique. Peut donner des précisions de date etc... selon le désir de l'utilisateur \n";
  reply2 += "------------------\n- __*recap__ :\n\t**Usage** : Affiche la liste des jeux et la notation donnée par l'utilisateur.\n\t**Arguments** : <discordUsername>(Optionnel) | Utilisateur cible. Par défaut sera celui qui a lancé la commande.\n";
  reply2 += "------------------\n- __*update__ :\n\t**Usage** : Permet à l'utilisateur de modifier sa note personnelle pour un jeu spécifique.\n\t**Arguments** : <nomDuJeu>(Obligatoire) | Nom du jeu normalisé (lettres minuscules et underscore)\n\t\t<nouvelleNote>(Obligatoire) | Nouvelle note à donner entre 0 et 10\n";

  message.author.send(reply2);

  let reply3 = "------------------\n- __*ranking__ :\n\t**Usage** : Affiche la liste des jeux triés par leur popularité auprès des joueurs enregistrés.\n\t**Arguments** : aucun\n";
  reply3 += "------------------\n- __*ban__ :\n\t**Usage** : Exclus un utilisateur de la liste des destinataires de vos futurs *call.\n\t**Arguments** : <idDiscord>(Obligatoire) | Clic-droit sur l'utilisateur > Copier l'identifiant\n";

  message.author.send(reply3);
}

/**
 * Commande d'aide aux commandes admin du bot
 * @param {*} message 
 */
function helpAdmin(message) {
  let reply = "**__Liste des commandes admin__**\n**Ces commandes ne sont exécutables que par des utilisateurs possédant des droits d'administration sur le serveur**\n";
  reply += "------------------\n- __*conf addGame__ :\n\t**Usage** : Fait enregistrer un nouveau jeu au bot.\n\t**Arguments** : <nomDuNouveauJeu>(Obligatoire) | En un seul mot, utiliser des underscores à la place des espaces\n\t\t<capaciteMax>(Optionnel) | Nombre de joueurs possible au max sur le jeu, 0 par défaut (car non pris en compte dans l'algo)\n";
  reply += "------------------\n- __*conf delGame__ :\n\t**Usage** : Supprime le jeu renseigné de la liste du bot. Notez que cela ne retire pas les notations données à ce jeu par les utilisateurs. Ainsi un rajout ultérieur ne nécessitera pas forcément d'interventions pour les utilisateurs lambdas.\n\t**Arguments** : <nomDuJeu>(Obligatoire) | En un seul mot, utiliser des underscores à la place des espaces\n";
  reply += "------------------\n- __*conf updateGame__ :\n\t**Usage** : Met à jour le jeu dans la liste du bot.\n\t**Arguments** : <nomDuJeu>(Obligatoire) | En un seul mot, utiliser des underscores à la place des espaces\n\t\t<capaciteMax>(Optionnel) | Nombre de joueurs possible au max sur le jeu, 0 par défaut (car non pris en compte dans l'algo)\n";
  message.author.send(reply);
}